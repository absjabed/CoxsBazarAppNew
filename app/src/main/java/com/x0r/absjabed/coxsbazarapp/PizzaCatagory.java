
package com.x0r.absjabed.coxsbazarapp;

class PizzaCatagory {

    final String name;

    final PizzaTopping[] toppings;

    public PizzaCatagory(
            final String name,
            final PizzaTopping... toppings) {
        
        this.name = name;
        this.toppings = toppings;
    }

}
