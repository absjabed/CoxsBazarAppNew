package com.x0r.absjabed.coxsbazarapp;

import android.animation.Animator;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    TextView cityField, detailsField, currentTemperatureField, humidity_field, pressure_field, weatherIcon, updatedField;

    Typeface weatherFont;

    public static int count=0,count1=0,count2=0,count3=0,count4=0;
    int[] drawablearray=new int[]{R.drawable.beach,R.drawable.beach1,R.drawable.beach2,R.drawable.beach4,R.drawable.beach5};
    //--------------Hotel Ads
    int[] drawablearray1=new int[]{R.drawable.hotel1,R.drawable.hotel5,R.drawable.hotel4,R.drawable.hotel8}; //0,4,3,7
    int[] drawablearray2=new int[]{R.drawable.hotel2,R.drawable.hotel6,R.drawable.hotel3,R.drawable.hotel7}; //1,5,2,6
    int[] drawablearray3=new int[]{R.drawable.hotel3,R.drawable.hotel7,R.drawable.hotel2,R.drawable.hotel6}; //2,6,1,5
    int[] drawablearray4=new int[]{R.drawable.hotel4,R.drawable.hotel8,R.drawable.hotel1,R.drawable.hotel5}; //3,7,0,4
    Timer _t,_t1,_t2,_t3,_t4;
    LinearLayout lnMain,lnhotelad1,lnhotelad2,lnhotelad3,lnhotelad4;
    ImageButton lifestyle,tour_travels,safety,hotel_motel,helpline,tidal_info,beach_activities,your_location,complain;
    ImageButton imfb,iminsta,imtwitter,imweb, imsettings;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.drawable.title_ixom);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        setContentView(R.layout.activity_main);


        //------------Weather fields
        weatherFont = Typeface.createFromAsset(getAssets(),"fonts/weathericons-regular-webfont.ttf");

        cityField = (TextView)findViewById(R.id.city_field);
        updatedField = (TextView)findViewById(R.id.updated_field);
        detailsField = (TextView)findViewById(R.id.details_field);
        currentTemperatureField = (TextView)findViewById(R.id.current_temperature_field);
        weatherIcon = (TextView)findViewById(R.id.weather_icon);
        weatherIcon.setTypeface(weatherFont);


        //------------Buttons
        lifestyle = (ImageButton) findViewById(R.id.blife_style);
        tour_travels = (ImageButton) findViewById(R.id.btour_travels);
        safety = (ImageButton) findViewById(R.id.bsafety);
        hotel_motel = (ImageButton) findViewById(R.id.bhotel);
        helpline = (ImageButton) findViewById(R.id.bhelpline);
        tidal_info = (ImageButton) findViewById(R.id.bunknown);
        beach_activities = (ImageButton) findViewById(R.id.bbeach);
        your_location = (ImageButton) findViewById(R.id.byourlocation);
        complain = (ImageButton) findViewById(R.id.bcomplain);
        imfb = (ImageButton) findViewById(R.id.bfacebook);
        iminsta = (ImageButton) findViewById(R.id.binstagram);
        imtwitter = (ImageButton) findViewById(R.id.btwitter);
        imweb= (ImageButton) findViewById(R.id.bweb);
        imsettings = (ImageButton) findViewById(R.id.bsettings);


        final Animation scalez = AnimationUtils.loadAnimation(this,R.anim.anim_scale);
        final Animation scale = AnimationUtils.loadAnimation(this,R.anim.flip_u);


        //----------Button Clicks------------------------
        lifestyle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(scale);
                Handler handler = new Handler(Looper.getMainLooper());  //---increases transition time
                final Runnable r = new Runnable() {                     //---after animation execution to switch activity
                    public void run() {
                        //do your stuff here after DELAY sec
                        Intent myIntent = new Intent(MainActivity.this, LifeStyle_Activity.class);
                        startActivity(myIntent);
                    }
                };
                handler.postDelayed(r, 1500);

            }
        });

        tour_travels.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(scale);
                Handler handler = new Handler(Looper.getMainLooper());  //---increases transition time
                final Runnable r = new Runnable() {                     //---after animation execution to switch activity
                    public void run() {
                        //do your stuff here after DELAY sec
                        Intent myIntent = new Intent(MainActivity.this, TourTravels_Activity.class);
                        startActivity(myIntent);
                    }
                };
                handler.postDelayed(r, 1500);
            }
        });

        safety.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(scale);
                Handler handler = new Handler(Looper.getMainLooper());  //---increases transition time
                final Runnable r = new Runnable() {                     //---after animation execution to switch activity
                    public void run() {
                        //do your stuff here after DELAY sec
                        Intent myIntent = new Intent(MainActivity.this, SafetySecurity_Activity.class);
                        startActivity(myIntent);
                    }
                };
                handler.postDelayed(r, 1500);
            }
        });

        hotel_motel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(scale);
                Handler handler = new Handler(Looper.getMainLooper());  //---increases transition time
                final Runnable r = new Runnable() {                     //---after animation execution to switch activity
                    public void run() {
                        //do your stuff here after DELAY sec
                        Intent myIntent = new Intent(MainActivity.this, HotelMotel_Activity.class);
                        startActivity(myIntent);
                    }
                };
                handler.postDelayed(r, 1500);
            }
        });

        helpline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(scale);
                Handler handler = new Handler(Looper.getMainLooper());  //---increases transition time
                final Runnable r = new Runnable() {                     //---after animation execution to switch activity
                    public void run() {
                        //do your stuff here after DELAY sec
                        Intent myIntent = new Intent(MainActivity.this, HelplineActivity.class);
                        startActivity(myIntent);
                    }
                };
                handler.postDelayed(r, 1500);
            }
        });

        tidal_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {  // Actually Tidal info activity...................
                v.startAnimation(scale);
                Handler handler = new Handler(Looper.getMainLooper());  //---increases transition time
                final Runnable r = new Runnable() {                     //---after animation execution to switch activity
                    public void run() {
                        //do your stuff here after DELAY sec
                       // Intent myIntent = new Intent(MainActivity.this, LifeStyle_Activity.class);
                        Intent myIntent = new Intent(MainActivity.this, TidalInfo_Activity.class);
                        startActivity(myIntent);
                    }
                };
                handler.postDelayed(r, 1500);
            }
        });

        beach_activities.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(scale);
                Handler handler = new Handler(Looper.getMainLooper());  //---increases transition time
                final Runnable r = new Runnable() {                     //---after animation execution to switch activity
                    public void run() {
                        //do your stuff here after DELAY sec
                        Intent myIntent = new Intent(MainActivity.this, BeachActivity.class);
                        startActivity(myIntent);
                    }
                };
                handler.postDelayed(r, 1500);
            }
        });

        your_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(scale);
                Handler handler = new Handler(Looper.getMainLooper());  //---increases transition time
                final Runnable r = new Runnable() {                     //---after animation execution to switch activity
                    public void run() {
                        //do your stuff here after DELAY sec
                        Intent myIntent = new Intent(MainActivity.this, YourLocation_Activity.class);
                        startActivity(myIntent);
                    }
                };
                handler.postDelayed(r, 1500);
            }
        });

        complain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(scale);
                Handler handler = new Handler(Looper.getMainLooper());  //---increases transition time
                final Runnable r = new Runnable() {                     //---after animation execution to switch activity
                    public void run() {
                        //do your stuff here after DELAY sec
                        Intent myIntent = new Intent(MainActivity.this, ComplainActivity.class);
                        startActivity(myIntent);
                    }
                };
                handler.postDelayed(r, 1500);
            }
        });

        imfb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(scalez);
                Handler handler = new Handler(Looper.getMainLooper());  //---increases transition time
                final Runnable r = new Runnable() {                     //---after animation execution to switch activity
                    public void run() {
                        //do your stuff here after DELAY sec
                        Intent webviewIntent = new Intent(MainActivity.this,WebView_Activity.class);
                        webviewIntent.putExtra("URL","https://www.facebook.com");
                        startActivity(webviewIntent);
                    }
                };
                handler.postDelayed(r, 1500);
            }
        });

        imtwitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(scalez);
                Handler handler = new Handler(Looper.getMainLooper());  //---increases transition time
                final Runnable r = new Runnable() {                     //---after animation execution to switch activity
                    public void run() {
                        //do your stuff here after DELAY sec
                        Intent webviewIntent = new Intent(MainActivity.this,WebView_Activity.class);
                        webviewIntent.putExtra("URL","https://twitter.com");
                        startActivity(webviewIntent);
                    }
                };
                handler.postDelayed(r, 1500);
            }
        });

        iminsta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(scalez);
                Handler handler = new Handler(Looper.getMainLooper());  //---increases transition time
                final Runnable r = new Runnable() {                     //---after animation execution to switch activity
                    public void run() {
                        //do your stuff here after DELAY sec
                        Intent webviewIntent = new Intent(MainActivity.this,WebView_Activity.class);
                        webviewIntent.putExtra("URL","https://www.instagram.com");
                        startActivity(webviewIntent);
                    }
                };
                handler.postDelayed(r, 1500);
            }
        });

        imweb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(scalez);
                Handler handler = new Handler(Looper.getMainLooper());  //---increases transition time
                final Runnable r = new Runnable() {                     //---after animation execution to switch activity
                    public void run() {
                        //do your stuff here after DELAY sec
                        Intent webviewIntent = new Intent(MainActivity.this,WebView_Activity.class);
                        webviewIntent.putExtra("URL","http://www.coxsbazar.gov.bd");
                        startActivity(webviewIntent);
                    }
                };
                handler.postDelayed(r, 1500);
            }
        });

        imsettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(scalez);
                Handler handler = new Handler(Looper.getMainLooper());  //---increases transition time
                final Runnable r = new Runnable() {                     //---after animation execution to switch activity
                    public void run() {
                        //do your stuff here after DELAY sec
                       // Toast toastz;
                        showCustomAlert();

                       // Toast.makeText(MainActivity.this,"", Toast.LENGTH_SHORT).show();

                    }
                };
                handler.postDelayed(r, 1500);
            }
        });


        //-------------------Button Clicks ends here...........


        //UI background changing code starts here----------------
        lnMain = (LinearLayout) findViewById(R.id.linlayout);
        _t = new Timer();
        _t.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {

                runOnUiThread(new Runnable() // run on ui thread
                {
                    public void run() {
                        if (count < drawablearray.length) {

                            lnMain.setBackgroundResource(drawablearray[count]);
                            count = (count + 1) % drawablearray.length;
                        }
                    }
                });
            }
        }, 5000, 5000);
        //UI background changing code ends here----------------


        //---------------------------Hotel Ads goes here----------------------------------//


        //-------------------------- Hotel Ad place 1 code starts
        lnhotelad1 = (LinearLayout) findViewById(R.id.hotelad1);
        _t1 = new Timer();
        _t1.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {                                             //0,4,3,7


                runOnUiThread(new Runnable() // run on ui thread
                {
                    public void run() {
                        if (count1 < drawablearray1.length) {

                            lnhotelad1.setBackgroundResource(drawablearray1[count1]);
                            count1 = (count1 + 1) % drawablearray1.length;
                        }

                        lnhotelad1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Drawable hb = lnhotelad1.getBackground();
                                String name = hb.toString().trim();
                                Toast.makeText(MainActivity.this, name, Toast.LENGTH_SHORT).show();
                                Intent i = new Intent(MainActivity.this, Ad_Activity.class);
                                i.putExtra("drawableback", name);
                                startActivity(i);
                            }
                        });

                    }
                });

            }
        }, 7000, 7000);
                        //Hotel ad place 1 ends ----------------


        //------------------------- Hotel Ad place 2 code starts
        lnhotelad2 = (LinearLayout) findViewById(R.id.hotelad2);
        _t2 = new Timer();
        _t2.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {                                              //1,5,2,6

                runOnUiThread(new Runnable() // run on ui thread
                {
                    public void run() {
                        if (count2 < drawablearray2.length) {

                            lnhotelad2.setBackgroundResource(drawablearray2[count2]);
                            count2 = (count2 + 1) % drawablearray2.length;
                        }
                        lnhotelad2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Drawable hb = lnhotelad2.getBackground();
                                String name = hb.toString().trim();
                                Toast.makeText(MainActivity.this, name, Toast.LENGTH_SHORT).show();
                                Intent i = new Intent(MainActivity.this, Ad_Activity.class);
                                i.putExtra("drawableback", name);
                                startActivity(i);
                            }
                        });

                    }
                });
            }
        }, 8000, 8000);
                        //Hotel ad place 2 ends ----------------

        //------------------------- Hotel Ad place 3 code starts
        lnhotelad3 = (LinearLayout) findViewById(R.id.hotelad3);
        _t3 = new Timer();
        _t3.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {                                              //2,6,1,5

                runOnUiThread(new Runnable() // run on ui thread
                {
                    public void run() {
                        if (count3 < drawablearray3.length) {

                            lnhotelad3.setBackgroundResource(drawablearray3[count3]);
                            count3 = (count3 + 1) % drawablearray3.length;
                        }
                        lnhotelad3.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Drawable hb = lnhotelad3.getBackground();
                                String name = hb.toString().trim();
                                Toast.makeText(MainActivity.this, name, Toast.LENGTH_SHORT).show();
                                Intent i = new Intent(MainActivity.this, Ad_Activity.class);
                                i.putExtra("drawableback", name);
                                startActivity(i);
                            }
                        });
                    }
                });
            }
        }, 6000, 6000);
        //Hotel ad place 3 ends ----------------


        //------------------------- Hotel Ad place 4 code starts
        lnhotelad4 = (LinearLayout) findViewById(R.id.hotelad4);
        _t4 = new Timer();
        _t4.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {                                                 //3,7,0,4

                runOnUiThread(new Runnable() // run on ui thread
                {
                    public void run() {
                        if (count4 < drawablearray4.length) {

                            lnhotelad4.setBackgroundResource(drawablearray4[count4]);
                            count4 = (count4 + 1) % drawablearray4.length;
                        }
                        lnhotelad4.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Drawable hb = lnhotelad4.getBackground();
                                String name = hb.toString().trim();
                                Toast.makeText(MainActivity.this, name, Toast.LENGTH_SHORT).show();
                                Intent i = new Intent(MainActivity.this, Ad_Activity.class);
                                i.putExtra("drawableback", name);
                                startActivity(i);
                            }
                        });
                    }
                });
            }
        }, 4000, 4000);
        //Hotel ad place 4 ends ----------------
        //---------------------------Hotel Ads ends here----------------------------------//

        //------------ Announces text-----------
        TextView textView = (TextView)findViewById(R.id.tx);
        textView.setSelected(true);
        textView.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        textView.setSingleLine(true);
        //--------------------------------------


        Function.placeIdTask asyncTask =new Function.placeIdTask(new Function.AsyncResponse() {
            public void processFinish(String weather_city, String weather_description, String weather_temperature, String weather_humidity, String weather_pressure, String weather_updatedOn, String weather_iconText, String sun_rise) {

                cityField.setText(weather_city);
                updatedField.setText(weather_updatedOn);
                detailsField.setText(weather_description);
                currentTemperatureField.setText(weather_temperature);
                weatherIcon.setText(Html.fromHtml(weather_iconText));

            }
        });
        asyncTask.execute("21.453239", "91.979767"); //  asyncTask.execute("Latitude", "Longitude")

    }
    public void showCustomAlert()
    {
        Context context = getApplicationContext();
        // Create layout inflator object to inflate toast.xml file
        LayoutInflater inflater = getLayoutInflater();

        // Call toast.xml file for toast layout
        View toastv = inflater.inflate(R.layout.toast, null);

        Toast toast = new Toast(context);

        // Set layout to toast
        toast.setView(toastv);
        toast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL,
                0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.show();
    }
}
