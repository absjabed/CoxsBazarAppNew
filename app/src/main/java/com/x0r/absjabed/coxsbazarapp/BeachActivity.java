package com.x0r.absjabed.coxsbazarapp;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class BeachActivity extends AppCompatActivity {

    BaseAdapter baseAdapter;
   // Button beachBike, jetSki, paraSeiling, photography, cultural;
  // public static final String[] beachActivities = new String[] { "Beach Bike",
    //       "Jet Ski", "Para Sailing", "Photographic Events", "Cultural Events" };



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.drawable.title_ixom);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        setContentView(R.layout.activity_beach);

        ListView beachActivityListView;
        final ArrayList<JetshiItem> arrayList;

        beachActivityListView = (ListView) findViewById(R.id.beachActivitylist);
        arrayList = new ArrayList<>();

        arrayList.add(new JetshiItem("Beach Bike"));
        arrayList.add(new JetshiItem("Jet Ski"));
        arrayList.add(new JetshiItem("Para Sailing"));
        arrayList.add(new JetshiItem("Photographic Events"));
        arrayList.add(new JetshiItem("Cultural Events"));

        baseAdapter = new BaseAdapter() {

            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            @Override
            public View getView(int position, View view, ViewGroup viewGroup) {

                if (view == null) {

                    view = inflater.inflate(R.layout.new_list_item, null);
                }

                TextView nameTextView=(TextView) view.findViewById(R.id.desc);

                nameTextView.setText(arrayList.get(position).getJetname());

                return view;
            }

            @Override
            public long getItemId(int position) {
                // TODO Auto-generated method stub
                return 0;
            }

            @Override
            public Object getItem(int position) {

                return arrayList.get(position);
            }

            @Override
            public int getCount() {
                // TODO Auto-generated method stub
                return arrayList.size();
            }
        };

        beachActivityListView.setAdapter(baseAdapter);

        beachActivityListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch(position) {
                    case 0:
                        Intent intent = new Intent(BeachActivity.this,BeachBike_Activity.class);
                        startActivity(intent);
                        break;
                    case 1:
                        Intent intent1 = new Intent(BeachActivity.this,JetSki_Activity.class);
                        startActivity(intent1);
                        break;
                    case 2:
                        Intent intent2 = new Intent(BeachActivity.this,ParaSailing_Activity.class);
                        startActivity(intent2);
                        break;
                    case 3:
                        Intent intent3 = new Intent(BeachActivity.this,PhotographicEvents_Activity.class);
                        startActivity(intent3);
                        break;
                    case 5:
                        Intent intent4 = new Intent(BeachActivity.this,CulturalEvent_Activity.class);
                        startActivity(intent4);
                        break;
                }
            }
        });

       /* beachBike = (Button) findViewById(R.id.beachbike);
        jetSki = (Button) findViewById(R.id.jetski);
        paraSeiling = (Button) findViewById(R.id.peraSeiling);
        photography = (Button) findViewById(R.id.photography);
        cultural = (Button) findViewById(R.id.culturalEvents);


        beachBike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BeachActivity.this,BeachBike_Activity.class);
                startActivity(intent);
            }
        });
        jetSki.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BeachActivity.this,JetSki_Activity.class);
                startActivity(intent);
            }
        });
        paraSeiling.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BeachActivity.this,ParaSailing_Activity.class);
                startActivity(intent);
            }
        });
        photography.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BeachActivity.this,PhotographicEvents_Activity.class);
                startActivity(intent);
            }
        });
        cultural.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BeachActivity.this,CulturalEvent_Activity.class);
                startActivity(intent);
            }
        });*/


    }
    // create an action bar button
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.mymenu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    // handle button activities
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.Sharebutton) {
            // do something here
        }
        if (id == R.id.Informationbutton) {
            // do something here
        }
        return super.onOptionsItemSelected(item);
    }
}
