package com.x0r.absjabed.coxsbazarapp;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class SafetySecurity_Activity extends AppCompatActivity {
    BaseAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.drawable.title_ixom);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        setContentView(R.layout.activity_safety_security_);



        ListView ainshriListview;

        final ArrayList<JetshiItem> arrayList;


        ainshriListview =(ListView) findViewById(R.id.ainshriListview);
        arrayList = new ArrayList<>();

        arrayList.add(new JetshiItem("POLICE"));
        arrayList.add(new JetshiItem("RAB"));
        arrayList.add(new JetshiItem("MAGISTRATE"));
        arrayList.add(new JetshiItem("D.C."));
        arrayList.add(new JetshiItem("A.D.C"));
        arrayList.add(new JetshiItem("FIRE SERVICE "));
        arrayList.add(new JetshiItem("LIFE GOURD"));


        adapter = new BaseAdapter() {

            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            @Override
            public View getView(int position, View view, ViewGroup viewGroup) {

                if (view == null) {

                    view = inflater.inflate(R.layout.new_list_item, null);
                }

                TextView nameTextView=(TextView) view.findViewById(R.id.desc);
               // TextView mobileTextView=(TextView) view.findViewById(R.id.jetskimobileTextView);

                nameTextView.setText(arrayList.get(position).getJetname());

               // mobileTextView.setText(arrayList.get(position).getMobilenumber());

                return view;
            }

            @Override
            public long getItemId(int position) {
                // TODO Auto-generated method stub
                return 0;
            }

            @Override
            public Object getItem(int position) {

                return arrayList.get(position);
            }

            @Override
            public int getCount() {
                // TODO Auto-generated method stub
                return arrayList.size();
            }
        };

        ainshriListview.setAdapter(adapter);

        ainshriListview.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View view,
                                           int position, long ID) {


                Toast.makeText(getBaseContext(), "Item long pressed!", Toast.LENGTH_SHORT).show();

                return false;
            }
        });
        ainshriListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch(position) {
                    case 0:
                        Intent intent = new Intent(SafetySecurity_Activity.this,BlankActivity.class);
                        startActivity(intent);
                        break;
                    case 1:
                        Intent intent1 = new Intent(SafetySecurity_Activity.this,BlankActivity.class);
                        startActivity(intent1);
                        break;
                    case 2:
                        Intent intent2 = new Intent(SafetySecurity_Activity.this,BlankActivity.class);
                        startActivity(intent2);
                        break;
                    case 3:
                        Intent intent3 = new Intent(SafetySecurity_Activity.this,BlankActivity.class);
                        startActivity(intent3);
                        break;
                    case 5:
                        Intent intent5 = new Intent(SafetySecurity_Activity.this,BlankActivity.class);
                        startActivity(intent5);
                        break;
                    case 9:
                        Intent intent9 = new Intent(SafetySecurity_Activity.this,BlankActivity.class);
                        startActivity(intent9);
                        break;
                }
            }
        });

    }

    // create an action bar button
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.mymenu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    // handle button activities
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.Sharebutton) {
            // do something here
        }
        if (id == R.id.Informationbutton) {
            // do something here
        }
        return super.onOptionsItemSelected(item);
    }
}
